//
//  main.cpp
//  sort
//
//  ref:https://www.runoob.com/w3cnote/ten-sorting-algorithm.html
//

#include <iostream>
#include <vector>

using namespace std;

// #define BUBBLE_SORT
//#define SELECT_SORT
//#define INSERT_SORT
//#define SHELL_SORT
#define MERGE_SORT

class Sort{
public:
    // 冒泡排序
    /*
        思路：每遍历一次，将最小值放到i的位置
        特点：时间复杂度（O(n**2)）,实现较简单
     */
    void BubbleSort(vector<int>& arr){
        int arr_size = arr.size();
        for (int i = 0; i < arr_size - 1; ++i){
            for (int j = i + 1; j < arr_size; ++j){
                if (arr[i] > arr[j]) {
                    Swap(arr, i, j);
                } //end if
            } //end second for loop
        } // end first for loop
    }  //end function
    
    void BubbleSortOptimization(vector<int>& arr){
        int arr_size = arr.size();
        bool swap_label;    //此标志为用于判断是否已经有序，若是不再往下遍历
        for (int i = 0; i < arr_size - 1; ++i){
            swap_label = false;
            for (int j = i + 1; j < arr_size; ++j){
                if (arr[i] > arr[j]) {
                    Swap(arr, i, j);
                    swap_label = true;
                } //end if
            } //end second for loop
            if (swap_label == false) {
                break;
            } // end if
        } // end first for loop
    }  //end function
    
    //选择排序
    /*
        思路：以最前面的一个元素为基准遍历一遍，找到最小元素
        特点：时间复杂度（O(n**2)）,非稳定排序
        适用：数量少
     */
    void select_Sort(vector<int>& arr){
        int arr_size = arr.size();
        for (int i = 0; i < arr_size - 1; i++){
            int MinPos = i;   //最小值位置
            for (int j = i + 1; j < arr_size; j++) {
                if (arr[MinPos] > arr[j]) {
                    MinPos = j;
                }
                Swap(arr, MinPos, i);
            } // end second for loop
        } //end first for loop
    } // end function
    
    void select_Sort_Optimization(vector<int>& arr){
        int arr_size = arr.size();
        for (int i = 0; i < arr_size / 2; i++){
            int MinPos = i;   //最小值位置
            int MaxPos = i;   //最大值位置
            for (int j = i + 1; j < arr_size - i; j++) {
                if (arr[MinPos] > arr[j]) {
                    MinPos = j;     //记录最小值下标
                }
                
                if (arr[MaxPos] < arr[j]){
                    MaxPos = j;     //记录最大值下标
                }

                if (MaxPos == MinPos){
                    break;
                }
                Swap(arr, MinPos, i);

                if (MaxPos == i){
                    MaxPos = MinPos;
                }

                int lastIndex = arr_size - 1 - i;
                Swap(arr, MaxPos, lastIndex);
            } // end second for loop
        } //end first for loop
    } // end function
    
    //插入排序
    /*
        思路：和抓牌后的整牌原理一样，从第2个元素开始遍历，cur为当前遍历到的元素值，与其前一个进行比较，直到比前一个大或者已经到第一个元素位置，将比cur大的数往后移位
        特点：时间复杂度（O(n**2)），会进行arr_size - 1轮比较
        适用：基本有序或数据量少
     */
    void insert_Sort(vector<int>& arr){
        int arr_size = arr.size();
//        从第二个数开始，往前加入数字
        for (int i = 1; i < arr_size; ++i) {
            int pos = i - 1;
            int cur = arr[i];
//            while循环中实现不断将比cur更大的数往后移，终止循环条件为
//              1.一旦遍历到arr[pos]小于或等于cur时，跳出循环，此时cur位置为arr[pos]位置后一位
//              2.pos已经到达了头部即pos = -1时，跳出循环，此时cur位置为数组头部位置
            while (pos >= 0 && cur < arr[pos]) {
                arr[pos + 1] = arr[pos];
                pos--;
            }   //end while loop
            arr[pos + 1] = cur;
        }   //end for loop
    }   //end function
    
    // 希尔排序
    /*
        思路：间隔分组+自定义排序
        特点：时间复杂度（nlogn）、非稳定排序、插入排序的优化
        适用：数据量大
        参考：https://zhuanlan.zhihu.com/p/87781731
     */
    void shell_Sort(vector<int>& arr){
        int arr_size = arr.size();
        int gap;     //一般gap为总长度的一半，直到gap值为1
        for (gap = arr_size / 2; gap > 0; gap /= 2){
//        从gap开始按照顺序将每个元素插入到自己所在的组
            for (int i = gap; i < arr_size; i++) {
                int preIndex = i - gap;     // preIndex为该组Index为i的上一个值索引
                int cur = arr[i];   // cur为Index为i对应的值
                while (preIndex >= 0 && arr[preIndex] > cur) {
                    Swap(arr, preIndex, i);     // 交换操作
                    preIndex -= gap;
                }   //end while
            }   //end for
        }   //end while
    }   //end function
    
    void shell_Sort1(vector<int>& arr){
        int arr_size = arr.size();
        for (int gap = arr_size / 2; gap > 0; gap /= 2) {
            for (int i = gap; i < arr_size; ++i){
                int cur = arr[i];
                int preIndex = i - gap;
                while (preIndex >= 0 && cur < arr[preIndex]) {
                    arr[preIndex + gap] = arr[preIndex];
                    preIndex -= gap;
                }
                arr[preIndex + gap] = cur;
            }   // end inner for loop
        }   //end outer for loop
    }   //end function
    
    void mysort(vector<int>& nums, int left, int right)
    {
        if (left < right){
            int mid = left + (right - left) / 2;
            mysort(nums, left, mid);
            mysort(nums, mid + 1, right);
            
            merge(nums, left, right);
        } //end if
        return;
    }   // end mysort
    
    /*
        思路：递归+分治法
        特点：时间复杂度为O(nlogn)，空间复杂度为O(n)
        适用：
        ref:https://segmentfault.com/a/1190000037644412
     */
    void mysort1(vector<int>& nums, int left, int right){
        if (left >= right) {
            return;
        }
        
        for (int i = left; i <= right; i++){
            cout << nums[i] << ' ';
        }
        cout << endl;
        
        int mid = left + (right - left) / 2;    //求这个值必须用这种写法，如果用(left+rigth)/2可能会造成overflow
        // 区间必须分成[left, mid]和[mid + 1, right]
        //若分成[left, mid - 1]和[mid, right]会导致无限递归，反例{1,2}用此区间进行排序在拆分时会分成[1, -1]和[0, 1]两个区间
        mysort1(nums, left, mid);
        mysort1(nums, mid + 1, right);
                
        merge(nums, left, right);
    }

    void merge(vector<int>& nums, int left, int right){
        vector<int> temp(nums.size());
        int mid = left + (right - left) / 2;
        int p = left;
        int q = mid + 1;
        int k = left;
//         下面3个while将子数组元素排序后存放至新数组temp中
        while (p <= mid && q <= right) {
            if (nums[p] < nums[q]) {
                temp[k++] = nums[p++];
            }else{
                temp[k++] = nums[q++];
            }   // end if-else
        }   // end while
        
        while (p <= mid) {
            temp[k++] = nums[p++];
        }   //end while
        
        while (q <= right) {
            temp[k++] = nums[q++];
        }   //end while
        
//        遍历一次将新数组中的元素copy至num数组中
        for (int i = left; i <= right; i++){
            nums[i] = temp[i];
        }
    }   // end merge
    
private:
    void Swap(vector<int>& arr, int i, int j){
        int t;
        t= arr[i];
        arr[i] = arr[j];
        arr[j] = t;
    }
};

int main(int argc, const char * argv[]) {
    vector<int> array = {3, 8, 2, 7, 9, 1, 10, 6, 5};
    cout << "原数组为：" << '{';
    for(int i = 0; i  < array.size(); ++i){
        cout << array[i] << ' ';
    }
    cout << '}' << endl;

#ifdef BUBBLE_SORT
    Sort bubble;
//    bubble.BubbleSort(array);
    bubble.BubbleSortOptimization(array);
    cout << "冒泡排序结果为：" << '{';
    for(int i = 0; i  < array.size(); ++i){
        cout << array[i] << ' ';
    }
    cout << '}' << endl;
#endif
 
#ifdef SELECT_SORT
    Sort selection;
    // selection.select_Sort(array);
    selection.select_Sort_Optimization(array);
    cout << "选择排序结果为：" << '{';
    for(int i = 0; i  < array.size(); ++i){
        cout << array[i] << ' ';
    }
    cout << '}' << endl;
#endif

#ifdef INSERT_SORT
    Sort insertion;
    insertion.insert_Sort(array);
    cout << "插入排序结果为：" << '{';
    for(int i = 0; i  < array.size(); ++i){
        cout << array[i] << ' ';
    }
    cout << '}' << endl;
#endif

#ifdef SHELL_SORT
    Sort shell;
    shell.shell_Sort(array);
//    shell.shell_Sort1(array);
    cout << "希尔排序结果为：" << '{';
    for(int i = 0; i  < array.size(); ++i){
        cout << array[i] << ' ';
    }
    cout << '}' << endl;
#endif
    
#ifdef MERGE_SORT
    Sort merge;
    merge.mysort1(array, 0, array.size() - 1);
//    merge.mysort(array, 0, array.size() - 1);
    cout << "归并排序结果为：" << '{';
    for(int i = 0; i  < array.size(); ++i){
        cout << array[i] << ' ';
    }
    cout << '}' << endl;
#endif
    return 0;
}
