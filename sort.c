#include <stdio.h>
#include <stdlib.h>

//#define MERGE_SORT
//#define BUBBLE_SORT
//#define SELECT_SORT
//#define INSERT_SORT
#define SHELL_SORT

void swap(int *arr, int i, int j){
    int t;
    t = arr[i];
    arr[i] = arr[j];
    arr[j] = t;
}

//冒泡排序
void Bubble(int *arr, int arr_size){
    for (int outer = 0; outer < arr_size - 1; ++outer) {
        for (int inner = outer + 1; inner < arr_size; ++inner) {
            if (arr[outer] > arr[inner]) {
                swap(arr, outer, inner);
            }
        }
    }   // end outer for
}

//选择排序
void Select(int *arr, int arr_size){
    for (int i = 0; i < arr_size - 1; i++) {
        int MinPos = i;
        for (int j = i + 1; j < arr_size; j++){
            if (arr[MinPos] > arr[j]) {
                MinPos = j;
            }   // end if
            swap(arr, MinPos, i);
        }   // end inner for loop
    }// end outer for loop
}

//插入排序
void Insert(int *arr, int arr_size){
    for (int i = 1; i < arr_size; i++){
        int pos = i - 1;
        int cur = arr[i];
        while (pos >= 0 && cur < arr[pos]) {
            arr[pos + 1] = arr[pos];
            pos--;
        }
        arr[pos + 1] = cur;
    }   // end for loop
}

//希尔排序
void Shell(int *arr, int arr_size){
    for (int gap = arr_size / 2; gap > 0; gap /= 2) {
        for (int i = gap; i < arr_size; i++) {
            int preIndex = i - gap;
            int cur = arr[i];
            while (preIndex >=0 && arr[preIndex] > arr[i]) {
                swap(arr, preIndex, i);
                preIndex -= gap;
            }
        }
    }   //end outer for loop
}

//归并排序
// 版本一
void Merge(int *arr, int left, int right){
    static int recursive_time = 0;
    recursive_time++;   //递归次数
    int left1 = left;
    int mid = left + (right - left) / 2;
    int right1 = mid;
    int left2 = mid + 1;
    int right2 = right;
    int arr_size = right - left;
    int temp[arr_size];
    
//    printf("当前的递归次数为：%d\n", recursive_time);
//    printf("mid = %d\t",mid);
//    printf("[left1 = %d, right1 = %d]\t", left1, right1);
//    printf("[left2 = %d, right2 = %d]\t", left2, right2);
//    printf("\n");
//    printf("当前left1和end1的元素值为：");
//    for(int i = left1; i <= right1; i++){
//        printf("%d\t", arr[i]);
//    }
//    printf("\n");
//    printf("当前left2和right2的元素值为：");
//    for(int j = left2; j <= right2; j++){
//        printf("%d\t", arr[j]);
//    }
//    printf("\n");
    
//    merge(arr, temp, left, left1, right1, left2, right2);
    int k = left;
    while (left1 <= right1 && left2 <= right2) {
//       temp[k++] = arr[i] > arr[j]? arr[j++] : arr[i++];
        if (arr[left1] < arr[left2]) {
            temp[k++] = arr[left1++];
        }else{
            temp[k++] = arr[left2++];
        }
    }

    while (left1 <= mid) {
        temp[k++] = arr[left1++];
    }

    while (left2 <= right) {
        temp[k++] = arr[left2++];
    }

    for (int l = left; l < right; ++l) {
        arr[l] = temp[l];
    }
    
//    printf("当前数组arr的值为 ");
    for (int p = left; p <= right; p++){
        arr[p] = temp[p];
//        printf("%d\t",arr[p]);
    }
//    printf("\n");
}

void MergeSort(int *arr, int left, int right){
    if (left >= right) {
        return;
    }
    
    //分
    int mid = left + (right - left) / 2;
    
    //治
    MergeSort(arr, left, mid);
    MergeSort(arr, mid + 1, right);
    
    //合
    Merge(arr, left, right);
}

//版本二
void merge(int *arr, int reg[], int start, int start1, int end1, int start2, int end2){
    int k = start;
    while (start1 <= end1 && start2 <= end2)
        reg[k++] = arr[start1] < arr[start2] ? arr[start1++] : arr[start2++];
    while (start1 <= end1)
        reg[k++] = arr[start1++];
    while (start2 <= end2)
        reg[k++] = arr[start2++];
}

void merge_sort_recursive(int *arr, int reg[], int start, int end) {
    if (start >= end)
        return;
    static int recursive_time = 0;
    recursive_time++;   //递归次数
    int len = end - start, mid = (len >> 1) + start;
    int start1 = start, end1 = mid;
    int start2 = mid + 1, end2 = end;
    
    //治
    merge_sort_recursive(arr, reg, start1, end1);
    merge_sort_recursive(arr, reg, start2, end2);
    
    //合
    merge(arr, reg, start, start1, end1, start2, end2);
//    int k = start;
//    while (start1 <= end1 && start2 <= end2)
//        reg[k++] = arr[start1] < arr[start2] ? arr[start1++] : arr[start2++];
//    while (start1 <= end1)
//        reg[k++] = arr[start1++];
//    while (start2 <= end2)
//        reg[k++] = arr[start2++];
    
    printf("当前数组arr的值为 ");
    for (int p = start; p <= end; p++){
        arr[p] = reg[p];
        printf("%d\t",arr[p]);
    }
    printf("\n");

}

void merge_sort(int *arr, const int len) {
    int reg[len];
    merge_sort_recursive(arr, reg, 0, len - 1);
}

int main(){
    int arr[] = {5, 4, 7, 10, 6, 9};
    int arr_len = sizeof(arr) / sizeof(arr[0]);
//    printf("数组arr的长度为%d\n",arr_len);
    
    printf("数组arr元素为：");
    for (int i = 0 ; i < arr_len; ++i) {
        printf("%d\t", arr[i]);
    }
    printf("\n");

#ifdef MERGE_SORT
    MergeSort(arr, 0, arr_len - 1);
//    merge_sort(arr, arr_len);
    printf("归并排序后数组arr元素为：");
    for (int i = 0 ; i < arr_len; ++i) {
        printf("%d\t", arr[i]);
    }
    printf("\n");
#endif
    
#ifdef BUBBLE_SORT
    Bubble(arr, arr_len);
    printf("冒泡排序后数组arr元素为：");
    for (int i = 0 ; i < arr_len; ++i) {
        printf("%d\t", arr[i]);
    }
    printf("\n");
#endif
    
#ifdef SELECT_SORT
    Select(arr, arr_len);
    printf("选择排序后数组arr元素为：");
    for (int i = 0 ; i < arr_len; ++i) {
        printf("%d\t", arr[i]);
    }
    printf("\n");
#endif
    
#ifdef INSERT_SORT
    Insert(arr, arr_len);
    printf("插入排序后数组arr元素为：");
    for (int i = 0 ; i < arr_len; ++i) {
        printf("%d\t", arr[i]);
    }
    printf("\n");
#endif
    
#ifdef SHELL_SORT
    Shell(arr, arr_len);
    printf("希尔排序后数组arr元素为：");
    for (int i = 0 ; i < arr_len; ++i) {
        printf("%d\t", arr[i]);
    }
    printf("\n");
#endif
}
