import time
arr1 = [5, 4, 9, 2, 3]

class Sort:

    def BubbleSort(self, arr):
        time_start = time.time()
        arr_len = len(arr)
        for i in range(0, arr_len - 1):
            for j in range(i + 1, arr_len):
                if(j < arr_len):
                    if(arr[j] < arr[i]):
                        self.swap(arr, i, j)
        time_end = time.time()
        print("冒泡排序所用时间：{}".format(time_end - time_start))

    def BubbleSortOptimization(self, arr):
        time_start = time.time()
        arr_len = len(arr)
        for i in range(0, arr_len - 1):
            swap_label = False
            for j in range(i + 1, arr_len):
                if(j < arr_len):
                    if(arr[j] < arr[i]):
                        self.swap(arr, i, j)
                        swap_label = True
            if(swap_label == False):
                break
        time_end = time.time()
        print("冒泡排序所用时间：{}".format(time_end - time_start))

    def SelectSort(self, arr):
        time_start = time.time()
        arr_len = len(arr)
        for i in range(0, arr_len - 1):
            minPos = i
            for j in range(i + 1, arr_len):
                if(arr[minPos] > arr[j]):
                    minPos = j
                self.swap(arr, minPos, i)
        time_end = time.time()
        print("选择排序所用时间：{}".format(time_end - time_start))

    def InsertSort(self, arr):
        time_start = time.time()
        arr_len = len(arr)
        for i in range(1, arr_len):
            pos = i - 1
            cur = arr[i]
            while(pos >= 0 and cur < arr[pos]):
                arr[pos + 1] = arr[pos]
                pos -= 1
            arr[pos + 1] = cur
        time_end = time.time()
        print("插入排序所用时间：{}".format(time_end - time_start))

    def ShellSort(self, arr):
        time_start = time.time()
        arr_len = len(arr)
        gap = int(arr_len/2)
        while (gap > 0):
            for i in range(int(gap), arr_len):
                j = i
                while(j >= gap and arr[j] < arr[int(j - gap)]):
                    self.swap(arr, j, j - gap)
                    j = j - gap
            gap /= 2
        time_end = time.time()
        print("希尔排序所用时间：{}".format(time_end - time_start))



    def swap(self, arr, ele1, ele2):
        t = arr[ele1]
        arr[ele1] = arr[ele2]
        arr[ele2] = t

sort = Sort()
print("原列表为：{}".format(arr1))
# sort.BubbleSort(arr1)
sort.BubbleSortOptimization(arr1)
print("冒泡排序后的列表为：{}".format(arr1))
sort.SelectSort(arr1)
print("选择排序后的列表为：{}".format(arr1))
sort.InsertSort(arr1)
print("插入排序后的列表为：{}".format(arr1))
sort.ShellSort(arr1)
print("希尔排序后的列表为：{}".format(arr1))
# print("冒泡排序后的列表为：{}".format(arr1))

